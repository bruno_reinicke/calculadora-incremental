public class Multiplicação extends Operação {

	public Multiplicação() {
		super();
	}
	
	public double multiplicar(double x, double y) {
		return x * y;
	}
}
