public class Operação {
	
	private double info;
	private int i = 0;
	
	private String operaçao;
	private String array[];
	private Operação operação, prox;

	public Operação() {
		this.operação = null;
	}
	
	public void setOperaçao(String operaçao) {
		this.operaçao = operaçao;
	}
	
	public String getOperaçao() {
		return this.operaçao;
	}
	
	public void empilhar() {
		array = new String[this.operaçao.length()];
		while (i < this.operaçao.length()) {
			
			if (this.operaçao.charAt(i) != ' ' && this.operaçao.charAt(i) != '+' && this.operaçao.charAt(i) != '/' 
				&& this.operaçao.charAt(i) != '*' && this.operaçao.charAt(i) != '-') {
				array = this.operaçao.split(" ");
			}
	
			i++;
		}
		
		for (int i = 0; i < array.length; i++) {
			if (!array[i].equals("+") && !array[i].equals("/") && !array[i].equals("*") && !array[i].equals("-")) {
				insere(Double.parseDouble(array[i]));
			}
			
			if (array[i].equals("+")) {
				Adição adc = new Adição();
				Operação op = this.operação;
				
				double tmp = adc.somar(op.getProx().getInfo(), op.getInfo());
				this.operação = null;
				insere(tmp);
			}
			
			if (array[i].equals("/")) {
				Divisão div = new Divisão();
				Operação op = this.operação;
				
				double tmp = div.dividir(op.getProx().getInfo(), op.getInfo());
				this.operação = null;
				insere(tmp);
			}
			
			if (array[i].equals("*")) {
				Multiplicação mult = new Multiplicação();
				Operação op = this.operação;
				
				double tmp = mult.multiplicar(op.getProx().getInfo(), op.getInfo());
				this.operação = null;
				insere(tmp);
			}
			
			if (array[i].equals("-")) {
				Subtração sub = new Subtração();
				Operação op = this.operação;
				
				double tmp = sub.subtrair(op.getProx().getInfo(), op.getInfo());
				this.operação = null;
				insere(tmp);
			}
		}
	} 
	
	public void setInfo(double info) {
		this.info = info;
	}
	
	public double getInfo() {
		return this.info;
	}
	
	public void setProx(Operação prox) {
		this.prox = prox;
	}
	
	public Operação getProx() {
		return this.prox;
	}
	
	public void insere(double info) {
		
		Operação nova = new Operação();
		nova.setInfo(info);
		nova.setProx(this.operação);
		this.operação = nova;
	}
	
	public boolean imprime() {
		Operação op = this.operação;
		while (op != null) {
			System.out.print(op.getInfo() + " ");
			op = op.getProx();
		}
		
		return true;
	}
}