public class Divisão extends Operação {

	public Divisão() {
		super();
	}
	
	public double dividir(double x, double y) {
		return x / y;
	}
}
