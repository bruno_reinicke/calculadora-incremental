public class Subtração extends Operação {

	public Subtração() {
		super();
	}
	
	public double subtrair(double x, double y) {
		return x - y;
	}
}
