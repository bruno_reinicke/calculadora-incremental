public class OperaçãoMain {

	public static void main(String[] args) {
		
		Operação operation1 = new Operação();
		Operação operation2 = new Operação();
		
		Operação operation3 = new Operação();
		Operação operation4 = new Operação();
		Operação operation5 = new Operação();
		
		operation1.setOperaçao("2 3 + 5 *");
		operation2.setOperaçao("2.3 3.2 + 2 *");
		
		operation3.setOperaçao("10 3 5 + *");
		operation4.setOperaçao("3 5 + 10 *");
		operation5.setOperaçao("3 5 + 2 /");
		
		try {
			operation1.empilhar();
			operation2.empilhar();
		
			operation4.empilhar();
			operation5.empilhar();
			operation3.empilhar();
			
		} catch (NullPointerException e) {
			System.out.println("Erro: posição nula! ");
		}
		
		assert operation1.imprime() == true;
		assert operation2.imprime() == true;
		assert operation3.imprime() == true;
		
		assert operation4.imprime() == true;
		assert operation5.imprime() == true;
	}
}