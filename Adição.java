public class Adição extends Operação {
	
	public Adição() {
		super();
	}
	
	public double somar(double x, double y) {
		return x + y;
	}
}
